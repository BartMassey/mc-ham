// Copyright © 2016 Bart Massey

import java.util.function.*;

/**
 * Parse exception for move string.
 */
class MoveParseException extends Exception {
    /** Create an exception with message.
     *
     * @param msg   exception message
     */
    public MoveParseException(String msg) {
        super(msg);
    }
}

/**
  * MiniChess move
  * @author   Bart Massey
  */
public class Move
implements Comparable<Move>, Supplier<Move> {
    public int from_row, from_col, to_row, to_col;

    /** Default constructor. */
    public Move() {}

    public Move get() {
        return new Move();
    }

    /** Copy constructor.
     * 
     * @param move   move to copy
     */
    public Move(Move move) {
        this.become(move);
    }

    /** Become a copy of given move.
     * 
     * @param move   move to become
     */
    public void become(Move move) {
        from_row = move.from_row;
        from_col = move.from_col;
        to_row = move.to_row;
        to_col = move.to_col;
    }

    /** Given a column description, return the
     *  corresponding column number.
     *
     * @param c   character to parse
     * @result    column number
     */
    static int parseCol(char c)
        throws MoveParseException {
        if (c >= 'a' && c <= 'e')
            return c - 'a';
        throw new MoveParseException("invalid column");
    }

    /** Given a row description, return the
     *  corresponding column number.
     *
     * @param c   character to parse
     * @result    row number
     */
    static int parseRow(char c)
        throws MoveParseException {
        if (c >= '1' && c <= '6')
            return c - '1';
        throw new MoveParseException("invalid row");
    }

    /** Given a column number, return
     *  the corresponding column name.
     *
     * @param col   column number
     * @result      column name
     */
    static String colString(int col) {
        char bytes[] = new char[1];
        bytes[0] = (char)((int)'a' + col);
        return new String(bytes);
    }

    /** Given a row number, return
     *  the corresponding row name.
     *
     * @param row   row number
     * @result      row name
     */
    static String rowString(int row) {
        char bytes[] = new char[1];
        bytes[0] = (char)((int)'1' + row);
        return new String(bytes);
    }

    /** Make a new move from a text description.
     *
     * @param desc       descriptive text
     * @return           move described
     */
    public static Move fromString(String desc)
    throws MoveParseException {
        if (desc.length() != 5 || desc.charAt(2) != '-')
                throw new MoveParseException("invalid move format");
        Move m = new Move();
        m.from_col = parseCol(desc.charAt(0));
        m.from_row = parseRow(desc.charAt(1));
        m.to_col = parseCol(desc.charAt(3));
        m.to_row = parseRow(desc.charAt(4));
        return m;
    }

    /** Make a text description of a square.
     *
     * @return      square description
     */
    public String toString() {
        return
            colString(from_col) +
            rowString(from_row) +
            "-" +
            colString(to_col) +
            rowString(to_row);
    }

    public boolean equals(Move m) {
        return
            from_row == m.from_row && from_col == m.from_col &&
            to_row == m.to_row && to_col == m.to_col;
    }

    public int compareTo(Move m) {
        if (from_col != m.from_col)
            return from_col - m.from_col;
        if (from_row != m.from_row)
            return from_row - m.from_row;
        if (to_col != m.to_col)
            return to_col - m.to_col;
        if (to_row != m.to_row)
            return to_row - m.to_row;
        return 0;
    }

    /* testing */
    public static void main(String[] args)
        throws MoveParseException {
        System.out.println(Move.fromString("a1-e6"));
    }
}
