SRC = MCHam.java Move.java Player.java PlayerGreedy.java	\
      PlayerHuman.java PlayerNegamax.java PlayerRandom.java	\
      PlayerID.java PlayerNegamax.java State.java               \
      Client.java PlayerIMCS.java ObjectPool.java               \
      PlayerIMCSAccept.java PlayerIMCSOffer.java

MCHam.class: $(SRC)
	javac $(SRC)

clean:
	-rm -f *.class
