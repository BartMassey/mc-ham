// Copyright © 2016 Bart Massey

import java.lang.*;
import java.util.*;
import java.util.function.*;

/**
 * MiniChess object pool
 * @author   Bart Massey
 */
public class ObjectPool<T> {
    // http://stackoverflow.com/questions/299998
    Supplier<? extends T> ctor;
    LinkedList<T> pool = new LinkedList<T>();

    public ObjectPool(Supplier<? extends T> ctor) {
        this.ctor = ctor;
    }

    public T alloc() {
        int n = pool.size();
        if (n == 0)
            return ctor.get();
        return pool.pop();
    }

    public void free(T t) {
        pool.push(t);
    }
}
