// Copyright © 2016 Bart Massey

import java.util.*;

/**
  * MiniChess random player
  * @author   Bart Massey
  */
public class PlayerRandom extends Player {
    public static String name = "random";
    Random prng = new Random();

    /** Create a new random player.
     *
     * @param desc   arguments
     * @param side   side to play
     */
    public PlayerRandom(String[] desc, char side) {
        super(side);
    }

    /** Let the computer pick and make a random move in the current state.
     *
     * @param s   state to move in
     * @param m   move that led to s
     * @return    next move
     */
    public Move getMove(State s, Move m) {
        ArrayList<Move> ms = s.genMoves();
        Move n = ms.get(prng.nextInt(ms.size()));
        report = n.toString();
        return n;
    }
}
