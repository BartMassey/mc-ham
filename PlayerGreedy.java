// Copyright © 2016 Bart Massey

import java.util.*;

/** Compare the scores of states resulting from two moves. */
class CompareScores implements Comparator<Move> {
    State s;

    /** When creating a comparator, remember the state to eval in.
     *
     * @param s    state to remember
     */
    public CompareScores(State s) {
        this.s = s;
    }

    /** Comparison function.
     *
     * @param m1    move to compare
     * @param m2    move to compare
     * @return      ordering
     */
    public int compare(Move m1, Move m2) {
        double ss1 = s.move(m1).score();
        double ss2 = s.move(m2).score();
        if (ss1 < ss2)
            return -1;
        if (ss1 > ss2)
            return 1;
        return 0;
    }
}

/**
  * MiniChess greedy player
  * @author   Bart Massey
  */
public class PlayerGreedy extends Player {
    public static String name = "greedy";

    /** Create a new greedy player.
     *
     * @param desc   arguments
     * @param side   side to play
     */
    public PlayerGreedy(String[] desc, char side) {
        super(side);
    }

    /** Let the computer pick and make a random move in the current state.
     *
     * @param s   state to move in
     * @param m   move that led to s
     * @return    next move
     */
    public Move getMove(State s, Move m) {
        ArrayList<Move> ms = s.genMoves();
        Comparator<Move> compareScores = new CompareScores(s);
        Collections.shuffle(ms);
        Collections.sort(ms, compareScores);
        Move n = ms.get(0);
        report = n.toString();
        return n;
    }
}
