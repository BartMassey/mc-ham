#!/bin/sh
# Copyright © 2016 Bart Massey

TMP=/tmp/compare.$$
trap "rm -f $TMP" 0 1 2 3 15

COUNT=1
while true; do
    echo -n '.'
    java MCHam greedy negamax:3 >$TMP
    if [ "`tail -1 $TMP`" != B ]
    then
        echo ""
        echo "*" $COUNT
        mv $TMP surprise.$COUNT
        COUNT=`expr $COUNT + 1`
    fi
done
