// Copyright © 2016 Bart Massey

import java.io.*;
import java.nio.*;
import java.nio.file.*;
import java.util.*;

/**
 * MiniChess state
 * @author   Bart Massey
 */
public class State {
    int moveNum;
    char onMove;
    char square[][] = new char[5][6];
    static ObjectPool<Move> movePool = new ObjectPool<Move>(Move::new);
    static String major_white = "RNBQK";
    static String major_black = "kqbnr";
    static final int ONE_STEP = 1;
    static final int ALL_STEP = 2;
    static final int NO_CAPTURE = 1;
    static final int CAPTURE = 2;
    static final int ONLY_CAPTURE = 3;

    /** Opponent of given player.
     *
     * @param player   player to be opposed
     * @return         opposing player
     */
    public static char opponent(char player) {
        if (player == 'W')
            return 'B';
        if (player == 'B')
            return 'W';
        throw new Error("bad opponent");
    }

    /** Color of given piece.
     *
     * @param p   piece to check color
     * @return    color or '?'
     */
    public static char color(char p) {
        if (p >= 'A' && p <= 'Z')
            return 'W';
        if (p >= 'a' && p <= 'z')
            return 'B';
        return '?';
    }

    /** Create the standard starting minichess state.
     * 
     *  @return   starting state
     */
    public static State start() {
        State s = new State();
        
        s.moveNum = 1;
        s.onMove = 'W';
        for (int c = 0; c < 5; c++) {
            s.square[c][0] = major_white.charAt(c);
            s.square[c][1] = 'P';
            for (int r = 2; r < 4; r++)
                s.square[c][r] = '.';
            s.square[c][4] = 'p';
            s.square[c][5] = major_black.charAt(c);
        }
        return s;
    }

    /** Create a state from a text description.
     * 
     *  @param str   text description
     *  @return      new state
     */
    public static State fromString(String str) {
        State s = new State();
        
        String[] lines = str.split("\n");
        if (lines.length != 7)
            throw new Error("input file not 7 lines");
        String[] labels = lines[0].split(" ");
        if (labels.length != 2)
            throw new Error("state header not 2 words");
        s.moveNum = Integer.parseInt(labels[0]);
        s.onMove = labels[1].charAt(0);
        if (s.onMove != 'B' && s.onMove != 'W')
            throw new Error("bad onmove when reading state file");
        for (int r = 0; r < 6; r++)
            for (int c = 0; c < 5; c++)
                s.square[c][r] = lines[6-r].charAt(c);
        return s;
    }

    /** Copy factory method.
     *
     */
    public State copy() {
        State s = new State();
        s.moveNum = moveNum;
        s.onMove = onMove;
        for (int c = 0; c < 5; c++)
            for (int r = 0; r < 6; r++)
                s.square[c][r] = square[c][r];
        return s;
    }

    /** Create a text description of a state.
     * 
     *  @return   text description.
     */
    public String toString() {
        String s = "";
        s += Integer.toString(moveNum);
        s += " ";
        s += Character.toString(onMove);
        s += "\n";
        for (int r = 5; r >= 0; --r) {
            for (int c = 0; c < 5; c++)
                s += Character.toString(square[c][r]);
            s += "\n";
        }
        return s;
    }

    /** Write state to given filename.
     * 
     *  @param fn   filename
     */
    public void toFile(String fn) throws IOException {
        FileWriter fw = new FileWriter(fn);
        fw.write(this.toString());
        fw.close();
    }
 
    /** Read state from given filename.
     * 
     *  @param fn   filename
     *  @return     new state
     */
    public static State fromFile(String fn) throws IOException {
        /* http://stackoverflow.com/a/326440 */
        byte[] bytes = Files.readAllBytes(Paths.get(fn));
        String str = new String(bytes);
        return State.fromString(str);
    }
 
    /** Return a new board with the given move made on it.
     *
     * @param m   move to be made
     * @return    new state with move made on it
     */
    public State move(Move m) {
        int fr = m.from_row;
        int fc = m.from_col;
        if (square[fc][fr] == '.')
            throw new Error("move from empty space");
        int tr = m.to_row;
        int tc = m.to_col;
        State s = this.copy();
        char p = s.square[fc][fr];
        if (p == 'p' && tr == 0 ||
            p == 'P' && tr == 5)
            p = 'Q';
        s.square[tc][tr] = p;
        s.square[fc][fr] = '.';
        s.onMove = opponent(onMove);
        if (s.onMove == 'W')
            s.moveNum++;
        return s;
    }

    /** Scan from a starting point in a direction looking
     *  for valid moves.
     *
     * @param moves     move list to append moves to
     * @param col       column to start at
     * @param row       row to start at
     * @param dcol      amount to step by in column direction
     * @param drow      amount to step by in row direction
     * @param step      step just once or as far as possible
     * @param capture   capture allowed, not-allowed or forced
     */
    void scan(ArrayList<Move> moves,
              int col, int row,
              int dcol, int drow,
              int step, int capture) {
        int start_col = col;
        int start_row = row;
        do {
            col += dcol;
            row += drow;
            if (col < 0 || col > 4 || row < 0 || row > 5)
                return;
            char p = square[col][row];
            if (p != '.' && color(p) == onMove)
                return;
            if (p == '.' && capture == ONLY_CAPTURE)
                return;
            if (p != '.' && capture == NO_CAPTURE)
                return;
            Move m = movePool.alloc();
            m.from_col = start_col;
            m.from_row = start_row;
            m.to_col = col;
            m.to_row = row;
            moves.add(m);
            if (p != '.')
                return;
        } while(step == ALL_STEP);
    }

    /** Scan in a direction, and in the three 90-degree
     *  rotations of that direction.
     *
     * @param moves     move list to append moves to
     * @param col       column to start at
     * @param row       row to start at
     * @param dcol      amount to step by in column direction
     * @param drow      amount to step by in row direction
     * @param step      step just once or as far as possible
     * @param capture   capture allowed, not-allowed or forced
     */
    void symm4(ArrayList<Move> moves,
               int col, int row,
               int dcol, int drow,
               int step, int capture) {
        for (int i = 0; i < 4; i++) {
            scan(moves, col, row, dcol, drow, step, capture);
            int tmp = dcol;
            dcol = drow;
            drow = -tmp;
        }
    }

    /** Add the moves for a piece at a given position.
     *
     * @param moves     move list to append moves to
     * @param col       column of position
     * @param row       row of position
     */
    public void addMovesForSquare(ArrayList<Move> moves,
                                  int col, int row) {
        char p = square[col][row];
        switch(p) {
        case 'K': case 'k':
            symm4(moves, col, row, 1, 0, ONE_STEP, CAPTURE);
            symm4(moves, col, row, 1, 1, ONE_STEP, CAPTURE);
            return;
        case 'Q': case 'q':
            symm4(moves, col, row, 1, 0, ALL_STEP, CAPTURE);
            symm4(moves, col, row, 1, 1, ALL_STEP, CAPTURE);
            return;
        case 'R': case 'r':
            symm4(moves, col, row, 1, 0, ALL_STEP, CAPTURE);
            return;
        case 'N': case 'n':
            symm4(moves, col, row, 1, 2, ONE_STEP, CAPTURE);
            symm4(moves, col, row, -1, 2, ONE_STEP, CAPTURE);
            return;
        case 'B': case 'b':
            symm4(moves, col, row, 1, 1, ALL_STEP, CAPTURE);
            symm4(moves, col, row, 1, 0, ONE_STEP, NO_CAPTURE);
            return;
        case 'P': case 'p':
            int dirn;
            switch (p) {
            case 'P': dirn = 1; break;
            case 'p': dirn = -1; break;
            default: throw new Error("internal pawn dirn error");
            }
            scan(moves, col, row, 0, dirn, ONE_STEP, NO_CAPTURE);
            scan(moves, col, row, -1, dirn, ONE_STEP, ONLY_CAPTURE);
            scan(moves, col, row, 1, dirn, ONE_STEP, ONLY_CAPTURE);
            return;
        }
        throw new Error("moved non-piece");
    }

    /** Generate all legal moves from a given state.
     *
     * @return    list of legal moves
     */
    public ArrayList<Move> genMoves() {
        ArrayList<Move> moves = new ArrayList<Move>();
        for (int c = 0; c < 5; c++)
            for (int r = 0; r < 6; r++)
                if (color(square[c][r]) == onMove)
                    addMovesForSquare(moves, c, r);
        return moves;
    }


    public static void freeMoves(ArrayList<Move> moves) {
        for (Move m : moves)
            movePool.free(m);
    }

    /** Report any result in the current state.
     *
     * @return   'W', 'B', '-', '?' as the state is white-win, black-win, draw, in-play
     */
    public char result() {
        boolean whiteKing = false;
        boolean blackKing = false;
        for (int c = 0; c < 5; c++)
            for (int r = 0; r < 6; r++)
                switch (square[c][r]) {
                case 'K': whiteKing = true; break;
                case 'k': blackKing = true; break;
                }
        if (!whiteKing && !blackKing)
            throw new Error("both kings missing");
        if (!whiteKing)
            return 'B';
        if (!blackKing)
            return 'W';
        if (moveNum > 40)
            return '-';
        if (genMoves().size() == 0)
            return opponent(onMove);
        return '?';
    }

    public final static double big = 1e9;

    static double center(int c, int r) {
        if (c > 0 && c < 4 && r > 0 && r < 5)
            return 0.02;
        return 0;
    }

    public double score() {
        boolean whiteKing = false;
        boolean blackKing = false;
        double t = 0;
        for (int c = 0; c < 5; c++) {
            for (int r = 0; r < 6; r++) {
                char p = square[c][r];
                if (p == '.')
                    continue;
                double s;
                switch (p) {
                case 'K': whiteKing = true; continue;
                case 'k': blackKing = true; continue;
                case 'P': s = 0.98 + 0.02 * r; break;
                case 'p': s = 0.98 + 0.02 * (5 - r); break;
                case 'Q': case 'q': s = 5; break;
                case 'R': case 'r': s = 3; break;
                case 'N': case 'n': s = 2 + center(c, r); break;
                case 'B': case 'b': s = 2.5 + center(c, r); break;
                default: throw new Error("mystery piece score");
                }
                if (color(p) == onMove)
                    t += s;
                else
                    t -= s;
            }
        }
        if (!whiteKing && !blackKing)
            throw new Error("both kings missing");
        if (!whiteKing) {
            if (onMove == 'W')
                return -big;
            return big;
        }
        if (!blackKing) {
            if (onMove == 'B')
                return -big;
            return big;
        }
        if (moveNum >= 41)
            return 0;
        if (genMoves().size() == 0)
            return -big;
        return t;
    }

    /** Generate a string containing the given moves, one per line.
     *
     * @param ms   moves to stringify
     * @return     stringified moves
     */
    public static String movesString(ArrayList<Move> ms) {
        String result = "";
        for (Move m : ms)
            result += m.toString() + "\n";
        return result;
    }

    /** Compare states for equality.
     *
     * @param s   state to compare with
     * @return    comparison
     */
    public boolean equals(State s) {
        if (moveNum != s.moveNum)
            return false;
        if (onMove != s.onMove)
            return false;
        for (int c = 0; c < 5; c++)
            for (int r = 0; r < 6; r++)
                if (square[c][r] != s.square[c][r])
                    return false;
        return true;
    }

    /** Test that the starting state is generated correctly,
     *  that the file writer and reader preserve the state,
     *  that the copy constructor works, and that a move
     *  does the right thing.
     */
    static void testMover() throws IOException, MoveParseException {
        State start = State.start();
        start.toFile("/tmp/test");
        State restart = State.fromFile("/tmp/test");
        Move m = Move.fromString("d2-d3");
        State saved = restart.copy();
        State fin = restart.move(m);
        System.out.print(fin);
        ArrayList<Move> ms = fin.genMoves();
        Collections.sort(ms);
        System.out.print(movesString(ms));
        if (!saved.equals(restart))
            throw new Error("copy/move error");
    }

    /** Generate the moves from the given state file.
     *
     * @param fn   state filename
     */
    static void testMovegen(String fn) throws IOException {
        State s = fromFile(fn);
        System.out.print(s);
        ArrayList<Move> ms = s.genMoves();
        Collections.sort(ms);
        System.out.print(movesString(ms));
    }

    /** Generate the score of the given state file.
     *
     * @param fn   state filename
     */
    static void testScore(String fn) throws IOException {
        State s = fromFile(fn);
        System.out.print(s);
        System.out.println(s.score());
    }

    /* Test code. */
    public static void main(String[] args)
        throws IOException, MoveParseException {
        if (args.length > 0 && args[0].equals("--test-mover"))
            testMover();
        else if (args.length > 1 && args[0].equals("--test-genmoves"))
            testMovegen(args[1]);
        else if (args.length > 1 && args[0].equals("--test-score"))
            testScore(args[1]);
        else throw new Error("unknown test");
    }
}
