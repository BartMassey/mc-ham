// Copyright © 2016 Bart Massey

import java.io.*;
import java.util.*;

/**
  * MiniChess iterative deepening negamax player
  * @author   Bart Massey
  */
public class PlayerID extends Player {
    public static String name = "id";
    double timeLimit = 5;
    PlayerNegamax negamaxEngine;
    Move moveChosen;

    /** Create a new ID player.
     *
     * @param desc   arguments
     * @param side   side to play
     */
    public PlayerID(String[] desc, char side) {
        super(side);
        negamaxEngine = new PlayerNegamax(side);
        if (desc.length == 1)
            return;
        if (desc.length == 2) {
            timeLimit = Double.parseDouble(desc[1]);
            return;
        }
        throw new Error("bad id arguments");
    }

    /** ID evaluator. Returns the negamax
     *  score of a given position after deepening
     *  for the given time.
     *
     * @param s   state to score
     * @param t   search time
     * @return    state score
     */
    double id(State s, double t) {
        double lastScore = negamaxEngine.negamax(s, 1);
        moveChosen = negamaxEngine.moveChosen;
        double endTime = PlayerNegamax.now() + t;
        // maximum depth before end of game
        int maxd = 81 - 2 * s.moveNum;
        if (s.onMove == 'W')
            maxd++;
        int d = 2;
        while (d <= maxd) {
            double score;
            try {
                score = negamaxEngine.negamax(s, d, endTime);
            } catch (TimeoutException e) {
                return lastScore;
            }
            String remaining =
              String.format("%.2f", endTime - PlayerNegamax.now());
            String sscore = scoreFormat(score);
            System.out.println(negamaxEngine.moveChosen + " (" +
                               remaining + ":" +
                               d + "@" + sscore + ")");
            // found a loss, so return last non-loss move
            if (score <= -State.big + 1000)
                return lastScore;
            moveChosen = negamaxEngine.moveChosen;
            lastScore = score;
            // found a win, so return winning move
            if (score >= State.big - 1000)
                return score;
            d++;
        }
        return lastScore;
    }

    /** Let the computer pick and make an id move in the current state.
     *
     * @param s   state to move in
     * @param m   move that led to s
     * @return    next move
     */
    public Move getMove(State s, Move m) {
        moveChosen = null;
        double score = id(s, timeLimit);
        report = PlayerNegamax.moveScoreFormat(moveChosen, score);
        return moveChosen;
    }
}
