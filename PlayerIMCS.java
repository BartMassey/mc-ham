// Copyright © 2016 Bart Massey

import java.io.*;
import java.util.*;

/**
  * MiniChess IMCS player
  * @author   Bart Massey
  */
abstract public class PlayerIMCS extends Player {
    public static String name = null;
    Client client;
    String serverName = "imcs-wurzburg.svcs.cs.pdx.edu";
    String serverPort = "23";
    String username = "mc-ham";
    String password = "ham";

    /** Create a new IMCS player.
     *
     * @param desc   arguments
     * @param side   side to play
     */
    public PlayerIMCS(char side) {
        super(side);
    }

    /** Let IMCS pick and make a move in the current state.
     *
     * @param s   state to move in
     * @param m   move that led to s
     * @return    next move
     */
    public Move getMove(State s, Move m) {
        Move n;
        try {
            if (m != null)
                client.sendMove(m.toString());
            String ns = client.getMove();
            if (ns == null)
                n = null;
            else
                try {
                    n = Move.fromString(ns);
                } catch (MoveParseException e) {
                    throw new Error("illegal move from client");
                }
        } catch (IOException e) {
            throw new Error("IMCS problem");
        }
        if (n == null) {
            System.out.println("unexpected null move from IMCS");
            report = null;
        } else {
            report = n.toString();
        }
        return n;
    }

    /** Report final move of game to IMCS if needed.
     *
     * @param m   move that led to s
     */
    public void sendMove(Move m) {
        try {
            if (m != null)
                client.sendMove(m.toString());
            client.close();
        } catch (IOException e) {
            throw new Error("IMCS problem");
        }
    }
}
