// Copyright © 2016 Bart Massey

import java.io.*;
import java.util.*;

/**
  * MiniChess IMCS offer player
  * @author   Bart Massey
  */
public class PlayerIMCSOffer extends PlayerIMCS {
    public static String name = "offer";

    /** Create a new IMCS player.
     *
     * @param desc   arguments
     * @param side   side to play
     */
    public PlayerIMCSOffer(String[] desc, char side) {
        super(side);
        try {
            client = new Client(serverName, serverPort, username, password);
            client.offer(State.opponent(side));
        } catch (IOException e) {
            throw new Error("cannot connect client");
        }
    }
}
