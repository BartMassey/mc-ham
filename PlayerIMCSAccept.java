// Copyright © 2016 Bart Massey

import java.io.*;
import java.util.*;

/**
  * MiniChess IMCS accept player
  * @author   Bart Massey
  */
public class PlayerIMCSAccept extends PlayerIMCS {
    public static String name = "accept";

    /** Create a new IMCS player.
     *
     * @param desc   arguments
     * @param side   side to play
     */
    public PlayerIMCSAccept(String[] desc, char side) {
        super(side);
        try {
            client = new Client(serverName, serverPort, username, password);
            client.accept(desc[1], State.opponent(side));
        } catch (IOException e) {
            throw new Error("cannot connect client");
        }
    }
}
