// Copyright © 2016 Bart Massey

import java.io.*;
import java.util.*;

/**
  * MiniChess human player
  * @author   Bart Massey
  */
public class PlayerHuman extends Player {
    public static String name = "human";
    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    /** Create a new human player.
     *
     * @param desc   arguments
     * @param side   side to play
     */
    public PlayerHuman(String[] desc, char side) {
        super(side);
    }

    /** Let the human pick and make a move in the current state.
     *
     * @param s   state to move in
     * @param m   move leading to state
     * @return    next move
     */
    public Move getMove(State s, Move m) {
        ArrayList<Move> ms = s.genMoves();
        while (true) {
            System.out.print("? ");
            String mstr;
            try {
                mstr = input.readLine();
            } catch (IOException e) {
                System.out.println("I/O error");
                System.out.print(s);
                continue;
            }
            Move n;
            try {
                n = Move.fromString(mstr);
            } catch (MoveParseException e) {
                System.out.println("invalid move: try again");
                System.out.print(s);
                continue;
            }
            for (Move m0: ms)
                if (m0.equals(n))
                    return n;
            System.out.println("illegal move: try again");
            System.out.print(s);
        }
    }
}
