// Copyright © 2016 Bart Massey

/**
  * MiniChess abstract player
  * @author   Bart Massey
  */
abstract public class Player {
    char side;
    String report = null;

    /** Create a new player.
     *
     * @param side   side to play
     */
    public Player(char side) {
        this.side = side;
    }

    public static String scoreFormat(double s) {
        if (s >= State.big - 1000)
            return "+";
        if (s <= -State.big + 1000)
            return "-";
        return String.format("%.2f", s);
    }

    /** Pick a move in the current state.
     *
     * @param s   state to move in
     * @param m   move that led to this state
     * @return    next move
     */
    abstract public Move getMove(State s, Move m);

    /** Report final move of game if needed.
     *
     * @param m   move
     */
    public void sendMove(Move m) {
        /* do nothing */
    }
}
