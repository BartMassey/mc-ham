// Copyright © 2016 Bart Massey

import java.io.*;
import java.util.*;

/**
 * Timeout exception for negamax.
 */
class TimeoutException extends Exception {}

/**
 * MiniChess negamax player
 * @author   Bart Massey
 */
public class PlayerNegamax extends Player {
    public static String name = "negamax";
    int depth = 5;
    public Move moveChosen = null;
    boolean abCheck = false;
    double endTime = 0;
    double timecheckCount = 0;

    /** Create a new negamax player.
     *
     * @param desc   arguments
     * @param side   side to play
     */
    public PlayerNegamax(String[] desc, char side) {
        super(side);
        if (desc.length == 1)
            return;
        if (desc.length == 2) {
            depth = Integer.parseInt(desc[1]);
            if (depth < 0) {
                abCheck = true;
                depth = -depth;
            }
            return;
        }
        throw new Error("bad negamax arguments");
    }

    /** Create a new negamax player.
     *
     * @param side   side to play
     */
    public PlayerNegamax(char side) {
        super(side);
    }

    public static String moveScoreFormat(Move m, double s) {
        return m + " (" + scoreFormat(s) + ")";
    }

    /** Negamax evaluator. Returns the negamax
     *  score of a given position at a given depth.
     *
     * @param s         state to score
     * @param depth     search depth
     * @param top       is top-level (move-retrieval) call
     * @param a         alpha
     * @param b         beta
     * @param abPrune   do actual ab pruning if true
     * @return          state score
     */
    double negamax(State s, int depth, boolean top,
                   double a, double b, boolean abPrune)
      throws TimeoutException {
        if (s.result() != '?' || depth <= 0)
            return s.score();
        if (endTime > 0) {
            if (timecheckCount <= 0) {
                if (now() >= endTime)
                    throw new TimeoutException();
                timecheckCount = 1000;
            }
            --timecheckCount;
        }
        ArrayList<Move> ms = s.genMoves();
        if (top)
            Collections.shuffle(ms);
        boolean inited = false;
        double bestScore = -State.big;
        double aa = a;
        for (Move m : ms) {
            double score = -negamax(s.move(m), depth - 1, false,
                                    -b, -aa, abPrune);
            if (!inited || score > bestScore) {
                bestScore = score;
                if (top)
                    moveChosen = new Move(m);
                if (abPrune && score >= b) {
                    State.freeMoves(ms);
                    return score;
                }
                if (score > aa)
                    aa = score;
            }
            inited = true;
        }
        State.freeMoves(ms);
        return bestScore;
    }

    /** Negamax evaluator. Returns the negamax
     *  score of a given position at a given depth.
     *
     * @param s         state to score
     * @param depth     search depth
     * @return          state score
     */
    public double negamax(State s, int depth) {
        moveChosen = null;
        endTime = 0;
        try {
            return negamax(s, depth, true, -State.big, State.big, true);
        } catch (TimeoutException e) {
            throw new Error("unexpected timeout");
        }
    }

    /** Negamax evaluator. Returns the negamax
     *  score of a given position at a given depth
     *  or throws a timeout exception.
     *
     * @param s      state to score
     * @param depth  search depth
     * @param t      timeout time
     * @return       state score
     */
    public double negamax(State s, int depth, double t)
      throws TimeoutException {
        moveChosen = null;
        endTime = t;
        timecheckCount = 0;
        return negamax(s, depth, true, -State.big, State.big, true);
    }

    /** Let the computer pick and make a negamax move in the current state.
     *
     * @param s   state to move in
     * @param m   move that led to s
     * @return    next move
     */
    public Move getMove(State s, Move m) {
        moveChosen = null;
        endTime = 0;
        try {
            double score = negamax(s, depth, true,
                                   -State.big, State.big, true);
            if (abCheck) {
                double slowScore = negamax(s, depth, true,
                                           -State.big, State.big, false);
                if (slowScore != score) {
                    try {
                        s.toFile("abcheck.txt");
                    } catch (IOException e) {
                        throw new Error("could not write state file");
                    }
                    throw new Error("abCheck fail: " + slowScore + " " + score);
                }
            }
            report = moveScoreFormat(moveChosen, score);
            return moveChosen;
        } catch (TimeoutException e) {
            throw new Error("unexpected timeout");
        }
    }

    public static double now() {
        return (double) System.currentTimeMillis() / 1000.0;
    }
}
