#!/bin/sh
# Copyright © 2016 Bart Massey
# Run tests

mkdir -p tests/outputs

echo mover:
java State --test-mover >tests/outputs/state-mover.txt
diff tests/expected/state-mover.txt tests/outputs/state-mover.txt

echo genmoves:
for t in tests/expected/*-moves.txt
do
    TT=`basename $t`
    echo genmoves/$TT:
    sed '8,$d' <$t >tests/inputs/$TT
    java State --test-genmoves tests/inputs/$TT >tests/outputs/$TT
    diff $t tests/outputs/$TT
done

echo score:
for t in tests/expected/*-score.txt
do
    TT=`basename $t`
    echo score/$TT:
    sed '8,$d' <$t >tests/inputs/$TT
    java State --test-score tests/inputs/$TT >tests/outputs/$TT
    diff $t tests/outputs/$TT
done

echo search:
for t in tests/expected/*-search[1-9].txt
do
    TT=`basename $t`
    D=`echo $TT | sed 's/^.*-search\(.\).txt/\1/'`
    echo search/$TT:
    sed '8,$d' <$t >tests/inputs/$TT
    cp tests/inputs/$TT tests/outputs/$TT
    java MCHam --start tests/inputs/$TT \
       negamax:$D negamax:$D | tail -1 >>tests/outputs/$TT
    diff $t tests/outputs/$TT
done
