// Copyright © 2016 Bart Massey

import java.io.*;

/**
  * MiniChess program
  * @author   Bart Massey
  */
public class MCHam {

    static Player makePlayer(String desc, char side) {
        String[] descs = desc.split(":");
        if (PlayerHuman.name.equals(descs[0]))
            return new PlayerHuman(descs, side);
        if (PlayerRandom.name.equals(descs[0]))
            return new PlayerRandom(descs, side);
        if (PlayerGreedy.name.equals(descs[0]))
            return new PlayerGreedy(descs, side);
        if (PlayerNegamax.name.equals(descs[0]))
            return new PlayerNegamax(descs, side);
        if (PlayerID.name.equals(descs[0]))
            return new PlayerID(descs, side);
        if (PlayerIMCSAccept.name.equals(descs[0]))
            return new PlayerIMCSAccept(descs, side);
        if (PlayerIMCSOffer.name.equals(descs[0]))
            return new PlayerIMCSOffer(descs, side);
        throw new Error("unknown player type");
    }
    
    static String[] shift(int n, String[] args) {
        String[] result = new String[args.length - n];
        for (int i = n; i < args.length; i++)
            result[i - n] = args[i];
        return result;
    }

    /** Run a game between two opponents. */
    public static void main(String args[]) throws IOException {
        State s = State.start();
        if (args[0].equals("--start")) {
            s = State.fromFile(args[1]);
            args = shift(2, args);
        }
        Player white = makePlayer(args[0], 'W');
        Player black = makePlayer(args[1], 'B');
        Move m = null;
        char result;
        while ((result = s.result()) == '?') {
            System.out.println();
            System.out.print(s);
            String rep;
            switch (s.onMove) {
            case 'W': m = white.getMove(s, m); rep = white.report; break;
            case 'B': m = black.getMove(s, m); rep = black.report; break;
            default: throw new Error("mystery player");
            }
            if (m == null) {
                System.out.println("game ended unexpectedly");
                System.exit(1);
            }
            if (rep != null)
                System.out.println(rep);
            s = s.move(m);
        }
        System.out.println(m);
        System.out.println();
        System.out.print(s);
        System.out.println(result);
        switch (s.onMove) {
        case 'W': black.sendMove(null); white.sendMove(m); break;
        case 'B': white.sendMove(null); black.sendMove(m); break;
        default: throw new Error("mystery player");
        }
    }
}
